#!/bin/bash

#Usage:
#pem2dot.sh < cert_chain.pem | dot -Tpng > cert_chain.png


echo "digraph cert_chain {"
certtool -i \
	| awk -F': ' '
	BEGIN{delete linksrc; delete linkdst}
/^X.509 Certificate Information/ {n=n+1;  delete san}
/Issuer:/ {issuer = $2}
/Subject:/ {subject = $2}
/Not Before:/ {start = $2}
/Not After:/ {end = $2}
/DNSname:/ {san[length(san)]= $2}
/-----BEGIN CERTIFICATE-----/ {
	if(subject) {
		if(!map[subject]) {
			map[subject]=n
		} else {
			print "WARN: Ignoring duplicate entry for", subject >"/dev/stderr"
			next
		}
	} else {
		subject="(no subject)"
	}
	if(length(san)>0) {
		printf("%d [shape=Mrecord label=\"{%s", n, subject)
		for(i=0;i<length(san);i++) {
			printf("|%s",san[i])
		}
		printf("}\"]\n")
	} else {
		print n, "[shape=Mrecord label=\""subject"\"]"
	}
	linksrc[length(linksrc)]=issuer
	linkdst[length(linkdst)]=n
}
END {
for(i=0;i<length(linksrc);i++) {
	print map[linksrc[i]], "->", linkdst[i]
}
}'

echo  "}"
