pem2dot.sh: convert a pem file with multiple certificates into it into a dot diagram (to be rendered with graphviz)

Usage: 

    ./pem2dot.sh < certificates.pem | dot -Tpng > certificates.png

For pulling from remote hosts, a convenient wrapper script that calls openssl can be used:

    ./visualize_certchain.sh www.wpi.edu

