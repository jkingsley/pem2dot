#!/bin/bash

TARGET="$1"

echo Q |openssl s_client -host "$TARGET" -port 443 -prexit -showcerts | sed '/BEGIN/,/END/p' -n | "$(dirname "$0")/pem2dot.sh" |dot -Tpng > "$TARGET".png
